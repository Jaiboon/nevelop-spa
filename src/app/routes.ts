
import { Routes } from "@angular/router";
import { MainContentComponent } from "./main-content/main-content.component";
import { PricingComponent } from "./pricing/pricing.component";
import { PunnyComponent } from "./punny/punny.component";
import { SignInComponent } from "./sign-in/sign-in.component";
export const appRoutes: Routes = [
    { path: "home", component: MainContentComponent },
    { path: "pricing", component: PricingComponent },
    { path: "punny", component: PunnyComponent },
    { path: "signin", component: SignInComponent },
    
    { path: "**", redirectTo: "home", pathMatch: "full" }
];