/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PunnyComponent } from './punny.component';

describe('PunnyComponent', () => {
  let component: PunnyComponent;
  let fixture: ComponentFixture<PunnyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PunnyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PunnyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
